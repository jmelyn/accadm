# ~/.bashrc for root

# Don't change anything if not running in interactive mode
[[ $- != *i* ]] && return

# Set interactive mode of important commands
alias cp='/usr/bin/cp -i'
alias mv='/usr/bin/mv -i'
alias rm='/usr/bin/rm -i'

# Set color in grep
alias grep='/usr/bin/grep --color=auto'

# Set prompt
PS1='\[\e[41;01;37m\]\u@\h\[\e[00m\] \w # '
