# ~/.bash_profile for root

# Set path
export PATH=/usr/sbin:/usr/local/sbin:/usr/bin:/usr/local/bin

# Source ~/.bashrc
[[ -f ~/.bashrc ]] && source ~/.bashrc
