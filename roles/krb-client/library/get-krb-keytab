#!/bin/bash

# Script: get-kerberos-keytab
#

# If msktutil not installed
if [[ ! -x /usr/local/sbin/msktutil ]]
then
    # Create temporary directory
    msktutil_dir=~pkgbuild/msktutil
    rm -rf $msktutil_dir
    mkdir $msktutil_dir
    chown pkgbuild:pkgbuild $msktutil_dir
    cd $msktutil_dir

    # Change user to download and compile msktutil
    sudo -u pkgbuild bash -c '
        # Load and extract msktutil
        wget https://sourceforge.net/projects/msktutil/files/latest/download
        tar xjf download

        # Compile msktutil
        cd msktutil-*
        ./configure
        make
    '

    # Install msktutil and clean up
    cd msktutil-*
    make install
    rm -rf $msktutil_dir
    cd
fi

# Get first CERN KDC name
cern_kdc=$(dig -t SRV _kerberos._tcp.cern.ch | awk '/^cerndc/ {sub("[.][ \t].*", ""); print; exit}')

# Reset current Kerberos identity
kerberos_reset=$(curl -k --local-port 600-700 https://lxkerbwin.cern.ch/LxKerb.asmx/ResetComputerPassword?service=host)
reset_output=($(echo "$kerberos_reset" | awk ' \
        /^ +<success>/ {split($0, words, "[<>]"); success=words[3]}
        /^ +<hostname>/ {split($0, words, "[<>]"); hostname=words[3]}
        /^ +<computerpassword>/ {split($0, words, "[<>]"); password=words[3]}
        END {print success " " hostname " " password " "}'))

# Control reset output
if [[ ${reset_output[0]} != 'true' ]]
then
    echo 'FATAL: Kerberos reset failed' >&2
    exit 1
else
    krb_hostname=${reset_output[1]}
    krb_password=${reset_output[2]}
fi

# Create computer keytab (20sec after the reset)
sleep 20
/usr/local/sbin/msktutil \
    create \
    --dont-expire-password \
    --server $cern_kdc \
    --computer-name $krb_hostname \
    --old-account-password $krb_password

exit 0
