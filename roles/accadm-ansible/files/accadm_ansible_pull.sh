#!/bin/sh
#
# Name:     accadm_ansible_pull.sh
# Purpose:  Pull updates from the accadm repository, then call accadm_ansible.sh to play the playbook
# Usage:    accadm_ansible_pull.sh [<branch>] [<ansible-playbook options...>]

git_origin_path='https://:@gitlab.cern.ch:8443/jmelyn/accadm.git'
git_local_path='/opt/ansible/accadm'

# If first argument is the branch name
if (( $# > 0 )) && [[ ! $1 =~ ^- ]] && [[ ! $1 =~ *.yml ]]
then
    branch="$1"
    shift
fi

# If branch is not set yet
if [[ -z $branch ]]
then
    # If a branch already exists
    if [[ -d $git_local_path/.git ]]
    then
        branch=$(cd $git_local_path && git rev-parse --abbrev-ref HEAD)
    # Else default to master
    else
        branch=master
    fi
fi

# Delete $git_local_path if no git directory inside
[[ ! -d $git_local_path/.git ]] && rm -rf $git_local_path

# Clone ansible if $git_local_path does not exist
if [[ ! -d $git_local_path ]]
then
    mkdir $git_local_path
    if ! git clone -b $branch $git_origin_path $git_local_path
    then
        rm -rf $git_local_path
        echo "Failed to clone git project from $git_origin_path to $git_local_path" >&2
        exit 1
    fi
fi

# Move to the $git_local_path
if ! cd $git_local_path
then
    echo "Failed to move to Ansible directory $git_local_path" >&2
    exit 1
fi

# Update and remove any local changes
git fetch -p && git checkout $branch && git reset --hard origin/$branch && git clean -fdx && git gc

# Run ansible playbook
/opt/ansible/accadm/roles/accadm-ansible/files/accadm_ansible.sh "$@"
