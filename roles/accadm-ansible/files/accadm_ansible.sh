#!/bin/sh
#
# Name:     accadm-ansible.sh
# Purpose:  Run local version of the accadm ansible playbook with options giving behaviour equivalent to ansible-pull

# Force clean path
export PATH=/usr/sbin:/usr/bin

# Check ansible is not already running
if [[ $(pgrep -c ansible-playbook) != 0 ]]
then
    echo "FATAL: ansible-playbook is already running" >&2
    exit 1
fi

# Run the playbook
ansible_path=/opt/ansible/accadm
hostname=$(hostname -s)
echo ansible-playbook -i "$ansible_path/hosts" -l $hostname -c local "$@" "$ansible_path/local.yml"
     ansible-playbook -i "$ansible_path/hosts" -l $hostname -c local "$@" "$ansible_path/local.yml"
