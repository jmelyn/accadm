#!/bin/bash
#
# Name:     accadm_ansible_email.sh
# Purpose:  Extract errors from the logs of the Ansible runs on all hosts and send a summary email
#

# Create temp file
temp_file=/tmp/accadm_ansible_email.tmp
rm -f $tmp_file
today=$(date +%F)

# Fill temp file up with ansible logs with errors
for dir in /net/cs-ccr-accadm/srv/log
do
    log_path=$dir/ansible_log.txt
    if [[ -e $log_path ]] && [[ $(head -1 $log_path) == $today ]]
    then
        echo -e "\n+++ ${dir##/*} had an error during the accadm_ansible service:" >> $temp_file
        tail +2 $log_path >> $temp_file
    fi
done

# Send the mail to accadm admins
cat $temp_file | mail -s "Errors from accadm_ansible service" jean-michel.elyn@cern.ch

# Clean up
rm $temp_file
